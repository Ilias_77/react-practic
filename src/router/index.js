import About from "../pages/About";
import Error from "../pages/Error";
import Login from "../pages/Login";
import PostDetail from "../pages/PostDetail";
import Posts from "../pages/Posts";

export const privateRoutes = [
    {  path:"/", element: <About/>, exact: false },
    {  path:"/posts/", element: <Posts/>, exact: false },
    {  path:"/posts/:id", element: <PostDetail/>, exact: false },
    {  path:"/error", element: <Error/>, exact: false },
]
export const publicRoutes = [
    {  path:"/login", element: <Login/>, exact: true },
]