import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { privateRoutes, publicRoutes } from '../router'
import { useContext } from 'react'
import { AuthContext } from '../contect'

function AppRouter() {
  const {isAuth, isLoading} = useContext(AuthContext);

  if (isLoading) {
    return '';
  }
  
  return (
    isAuth ? 
      <Routes>
        {privateRoutes.map((route) =>
          <Route path={route.path} element={route.element} exact={route.exact} key={route.path}></Route>
        )} 
        <Route path="*" element={<Navigate to="/" replace />}/>
      </Routes>
      :
      <Routes>
        {publicRoutes.map((route) =>
          <Route path={route.path} element={route.element} exact={route.exact} key={route.path}></Route>
        )}
        <Route path="*" element={<Navigate to="/login" replace />}/>
      </Routes>
  )
}

export default AppRouter