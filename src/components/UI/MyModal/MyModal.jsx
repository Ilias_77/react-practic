import React from "react";
import style from "./MyModal.module.css";
function MyModal({ children, visible, setVisible }) {
  const modalClass = [style.myModal];
  if (visible) modalClass.push(style.active);

  return (
    <div className={modalClass.join(" ")} onClick={() => setVisible(false)}>
      <div
        className={style.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        {children}
      </div>
    </div>
  );
}

export default MyModal;
