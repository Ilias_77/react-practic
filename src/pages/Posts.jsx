import React, { useEffect, useState } from "react";
import "../styles/app.css";
import PostList from "../components/PostList";
import PostForm from "../components/PostForm";
import PostFilter from "../components/PostFilter";
import MyModal from "../components/UI/MyModal/MyModal";
import MyButton from "../components/UI/button/MyButton";
import { usePosts } from "../hooks/usePosts";
import PostService from "../API/PostService";
import Loader from "../components/UI/loader/Loader";
import { useFetching } from "../hooks/useFetching";
import { getPagesCount } from "../utils/pages";
import Pagination from "../components/UI/pagination/Pagination";
import { useRef } from "react";
import { useObserver } from "../hooks/useObserver";

function Posts() {
  const [posts, setPosts] = useState([]);
  const [ fetchPosts, isPostsLoading, postError ] = useFetching(async (limit, page) => {
    const response = await PostService.getAll(limit, page);
    setPosts([...posts, ...response.data]);
    setTotalPages( getPagesCount(response.headers['x-total-count'], limit));
  })
  const[totalPages, setTotalPages] = useState(0);
  const[limit, setLimit] = useState(10);
  const[page, setPage] = useState(1);
  const lastDomEl = useRef();
  
  const [filter, setFilter] = useState({ sort: "", query: "" });
 
  const sortedAndSearchedPosts = usePosts(posts, filter.query, filter.sort);
  
  const addNewPost = (post) => {
    setPosts([...posts, post]);
    setModal(false);
  };

  const removePost = (post) => {
    setPosts(posts.filter((p) => p.id !== post.id));
  };

  useEffect(() => {
    fetchPosts(limit,page);
  },[page])
  
  useObserver( lastDomEl, page < totalPages, isPostsLoading, () => setPage(page+1) );

  const changePage = (page) => {
    setPage(page);
  }

  const [modal, setModal] = useState(false);

  return (
    <div className="App">
      <MyButton onClick={() => setModal(true)}>Создать пост</MyButton>
      <MyModal visible={modal} setVisible={setModal}>
        <PostForm create={addNewPost} visible={true} />
      </MyModal>
      <hr />

      <PostFilter filter={filter} setFilter={setFilter} />
       {
        postError && <h2>Ошибка ${postError}</h2>
       }
      <hr />
      {
        isPostsLoading && <div style={{display: 'flex',
      justifyContent: 'center', margin: '10px'}}><Loader/></div>
      }
      <PostList
        posts={sortedAndSearchedPosts}
        title={"Список постов"}
        remove={removePost}
      />
      <div ref={lastDomEl} ></div>

      <Pagination totalPages={totalPages} page={page} changePage={changePage}></Pagination>
      
    </div>
  );
}

export default Posts;
