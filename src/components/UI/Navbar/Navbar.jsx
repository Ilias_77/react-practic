import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import styles from './Navbar.module.css'
import MyButton from '../button/MyButton'
import { AuthContext } from '../../../contect'

function Navbar() {
  const{isAuth, setIsAuth} = useContext(AuthContext);

  const logout = e => {
    setIsAuth(false);
    localStorage.removeItem('isAuth');
  }

  return (
    <div className={styles.navbar}>
        {isAuth ? <MyButton onClick={logout}>Выйти</MyButton> : ''}
        <div className={styles.navbarLinks}>
          <Link to="/about">О нас</Link>
          <Link to="/posts">Список записей</Link>
        </div>
      </div>      
  )
}

export default Navbar