import React from "react";
import PostItem from "./PostItem";

export default function PostList({ posts, title, remove }) {
  if (!posts.length) return <h2>Нет постов</h2>;

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>{title}</h1>
      {posts.map((post, index) => (
        <PostItem
          post={post}
          key={post.id}
          number={index + 1}
          remove={remove}
        />
      ))}
    </div>
  );
}
