import React from 'react'
import style from "./Pagination.module.css";
import { usePagination } from '../../../hooks/usePagination';

function Pagination({totalPages, page, changePage}) {
  const pagesArray = usePagination(totalPages);
  return (
    <div className={style.pagination}>
        {pagesArray.map((p)=>
        <div key={'page-'+p} onClick={() => changePage(p)} className={p === page ? 'pagination__item pagination__current' : 'pagination__item'}>{p}</div>
        )}
      </div>
  )
}

export default Pagination