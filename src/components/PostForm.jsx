import React, { useState } from "react";
import MyInput from "./UI/inpuit/MyInput";
import MyButton from "./UI/button/MyButton";

export default function PostForm({ create, ...props }) {
  const [post, setPost] = useState({ title: "", body: "" });

  const addNewPost = () => {
    create({ ...post, id: Date.now() });
    setPost({ title: "", body: "" });
  };

  return (
    <form>
      <MyInput
        value={post.title}
        onChange={(e) => setPost({ ...post, title: e.target.value })}
        type="text"
        placeholder="Название"
      />
      <MyInput
        value={post.body}
        onChange={(e) => setPost({ ...post, body: e.target.value })}
        type="text"
        placeholder="Описание"
      />
      <MyButton onClick={addNewPost} type="button">
        Создать
      </MyButton>
    </form>
  );
}
