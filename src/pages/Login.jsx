import React, { useContext } from 'react'
import MyInput from '../components/UI/inpuit/MyInput'
import MyButton from '../components/UI/button/MyButton'
import { AuthContext } from '../contect';

function Login() {
  const {isAuth, setIsAuth} = useContext(AuthContext);
  
  const login = e => {
    e.preventDefault();
    setIsAuth(true);
    localStorage.setItem('isAuth', 'true');
  }
  
  return (
    <div>
      <h1>Авторизация</h1>
      <form onSubmit={login}>

        <MyInput type="text" placeholder="Логин"/>
        <MyInput type="password" placeholder="Пароль"/>
        <MyButton>Войти</MyButton>
      </form>
    </div>
  )
}

export default Login