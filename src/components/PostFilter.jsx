import React from "react";
import MyInput from "./UI/inpuit/MyInput";
import MySelect from "./UI/select/MySelect";

function PostFilter({ filter, setFilter }) {
  return (
    <div>
      <MyInput
        placeholder=""
        value={filter.query}
        onChange={(e) => setFilter({ ...filter, query: e.target.value })}
      ></MyInput>

      <MySelect
        options={[
          { value: "title", name: "По названию" },
          { value: "body", name: "По описанию" },
        ]}
        value={filter.sort}
        onChange={(selectedSort) =>
          setFilter({ ...filter, sort: selectedSort })
        }
        defaultValue={"Сортировка по"}
      />
    </div>
  );
}

export default PostFilter;
