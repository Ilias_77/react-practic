import { useMemo } from 'react'

export const usePagination = (pagesCount) => {
  return useMemo(() => {
    let res = [];
    for (let i =0; i < pagesCount; i++) {
        res.push(i+1);
    }
    return res;
  },[pagesCount]);
}
