import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useFetching } from '../hooks/useFetching';
import PostService from '../API/PostService';
import Loader from '../components/UI/loader/Loader';

export default function PostDetail() {
  const params = useParams();
  const [post, setPost] = useState(null);
  const [comments, setComments] = useState([]);

  const [ fetchPost, isPostLoading, postError ] = useFetching(async (id) => {
    const response = await PostService.getById(id);
    setPost( response.data );
  })
  const [ fetchPostComments, isCommentsLoading, commentsError ] = useFetching(async (id) => {
    const response = await PostService.getCommentsByPostId(id);
    setComments( response.data );
  })

  useEffect( () => {
    fetchPost(params.id);
    fetchPostComments(params.id);
  },[]);

  return (
    <div className="App">
      <h1>Пост {params.id}</h1>
      {isPostLoading ? <Loader/> : 
        <section>
          <div>{post?.title}</div>
          <div>{post?.body}</div>
        </section>
      }

      {isCommentsLoading ? <Loader/> : 
        <section>
          <br/>
          <h2>Комментарии к посту</h2>
          {comments.map((comment) => 
            <div key={`${params.id}-${comment.id}`}>
              <p>{comment.name}</p>
              <p>{comment.email}</p>
              <p>{comment.body}</p>
              <br/>
            </div>
          )}
        </section>
      }
      
    </div>
  )
}
